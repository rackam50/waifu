﻿# TODO: Translation updated at 2019-11-27 12:19

# game/invest.rpy:4
translate Francais invest_b1dd594a:

    # p "Well... Nothing unusual from what I can see."
    p "Well... Nothing unusual from what I can see."

# game/invest.rpy:5
translate Francais invest_caca70a2:

    # mi "I thought you said Harada-sensei might be in here?"
    mi "I thought you said Harada-sensei might be in here?"

# game/invest.rpy:7
translate Francais invest_1efe799a:

    # p "He was here 5 minutes ago. Even though I didn't see him enter with my own eyes, we still should have run into him by now. There’s no other way to leave the locker rooms, after all."
    p "He was here 5 minutes ago. Even though I didn't see him enter with my own eyes, we still should have run into him by now. There’s no other way to leave the locker rooms, after all."

# game/invest.rpy:8
translate Francais invest_20fbd2cd:

    # mi "True... maybe he left by through a window, hehe."
    mi "True... maybe he left by through a window, hehe."

# game/invest.rpy:9
translate Francais invest_18ebad5f:

    # mi "If he did, I hope he was careful. They can shut on you without warning."
    mi "If he did, I hope he was careful. They can shut on you without warning."

# game/invest.rpy:10
translate Francais invest_904c97f4:

    # p "(You would know.)"
    p "(You would know.)"

# game/invest.rpy:12
translate Francais invest_0c6d76d9:

    # p "Look Minami-chan, the ball carriage is empty. I am certain it was full when I last saw him, and there are no volleyballs lying around in here."
    p "Look Minami-chan, the ball carriage is empty. I am certain it was full when I last saw him, and there are no volleyballs lying around in here."

# game/invest.rpy:13
translate Francais invest_7386271a:

    # mi "Mm... Unfortunately, the absence of evidence isn't evidence of absence, but it's definitely suspicious."
    mi "Mm... Unfortunately, the absence of evidence isn't evidence of absence, but it's definitely suspicious."

# game/invest.rpy:14
translate Francais invest_e4f59d41:

    # p "We must be overlooking something here, let's take a look around."
    p "We must be overlooking something here, let's take a look around."

# game/invest.rpy:16
translate Francais invest_9f991746:

    # "As you begin your search, you can almost hear the Monkey Island soundtrack playing in the background."
    "As you begin your search, you can almost hear the Monkey Island soundtrack playing in the background."

# game/invest.rpy:17
translate Francais invest_841da7db:

    # p "(Oh, LucasArts, what has become of thee?)"
    p "(Oh, LucasArts, what has become of thee?)"

# game/invest.rpy:27
translate Francais invest_start_223246b5:

    # "Search the area to find clues."
    "Search the area to find clues."

# game/invest.rpy:31
translate Francais invest_1_d70ba466:

    # p "These are just some cones with different colors. We sometimes used then for slalom, designating play areas, or makeshift trumpets in middle school. I also recall that they made for excellent wizard hats."
    p "These are just some cones with different colors. We sometimes used then for slalom, designating play areas, or makeshift trumpets in middle school. I also recall that they made for excellent wizard hats."

# game/invest.rpy:32
translate Francais invest_1_8eed2648:

    # p "There’s nothing else to see here."
    p "There’s nothing else to see here."

# game/invest.rpy:38
translate Francais invest_2_1d14b71f:

    # p "Mats, nothing terribly interesting…"
    p "Mats, nothing terribly interesting…"

# game/invest.rpy:39
translate Francais invest_2_e24b1749:

    # p "(Although, I am surprised that the stain Kaori made when we had some somnophillic fun came out.)"
    p "(Although, I am surprised that the stain Kaori made when we had some somnophillic fun came out.)"

# game/invest.rpy:40
translate Francais invest_2_fb7cd6fb:

    # p "But still, I'm pretty sure Asuka’s OCD wouldn't have tolerated the slight disorder they are in."
    p "But still, I'm pretty sure Asuka’s OCD wouldn't have tolerated the slight disorder they are in."

# game/invest.rpy:41
translate Francais invest_2_c882d7c6:

    # p "That must mean that Harada really came in here and moved the mats for some reason."
    p "That must mean that Harada really came in here and moved the mats for some reason."

# game/invest.rpy:42
translate Francais invest_2_caaa42cd:

    # mi "I see what you mean, but that’s really just speculation isn’t it?"
    mi "I see what you mean, but that’s really just speculation isn’t it?"

# game/invest.rpy:43
translate Francais invest_2_f90067e9:

    # p "You sure became skeptical quickly, didn’t you? Well, there’s nothing else to see here."
    p "You sure became skeptical quickly, didn’t you? Well, there’s nothing else to see here."

# game/invest.rpy:49
translate Francais invest_3_ef42ee64:

    # mi "These teeing platforms for driving practice hit against the door when we opened it, so they must have been moved from the inside."
    mi "These teeing platforms for driving practice hit against the door when we opened it, so they must have been moved from the inside."

# game/invest.rpy:50
translate Francais invest_3_0c4dfdb9:

    # mi "Curiouser and curiouser..."
    mi "Curiouser and curiouser..."

# game/invest.rpy:56
translate Francais invest_4_9f4c7b66:

    # mi "Huh? What are those marks on the ground...?"
    mi "Huh? What are those marks on the ground...?"

# game/invest.rpy:57
translate Francais invest_4_986becbb:

    # mi "[player_name]-kun, didn't you said that Asuka-chan cleaned in here today?"
    mi "[player_name]-kun, didn't you said that Asuka-chan cleaned in here today?"

# game/invest.rpy:59
translate Francais invest_4_806a19ff:

    # p "Yes, I didn't pay much attention to the quality of her work, but Asuka-chan wouldn't leave dirt on the ground like that."
    p "Yes, I didn't pay much attention to the quality of her work, but Asuka-chan wouldn't leave dirt on the ground like that."

# game/invest.rpy:60
translate Francais invest_4_77dbfdb3:

    # mi "So someone must have moved these vaulting boxes recently to make those marks."
    mi "So someone must have moved these vaulting boxes recently to make those marks."

# game/invest.rpy:61
translate Francais invest_4_146fbf67:

    # p "Let's try moving one..."
    p "Let's try moving one..."

# game/invest.rpy:63
translate Francais invest_4_bcff7f30:

    # "You tried to move the vaulting box next to the marks."
    "You tried to move the vaulting box next to the marks."

# game/invest.rpy:64
translate Francais invest_4_e22395f3:

    # p "*Argh* What the hell?! It won't move an inch."
    p "*Argh* What the hell?! It won't move an inch."

# game/invest.rpy:65
translate Francais invest_4_f34cc0aa:

    # mi "I can see that... I would offer my help, but it won't change anything, [player_name]-kun: it's fixed to the ground."
    mi "I can see that... I would offer my help, but it won't change anything, [player_name]-kun: it's fixed to the ground."

# game/invest.rpy:67
translate Francais invest_4_15423397:

    # "You look down and see that it is, indeed, bolted down. You silently curse yourself for losing some man points with your recklessness."
    "You look down and see that it is, indeed, bolted down. You silently curse yourself for losing some man points with your recklessness."

# game/invest.rpy:68
translate Francais invest_4_6d10514d:

    # p "So it is... Wait a minute, why would you fasten one of these to the floor in the storage room? You can’t use it like this."
    p "So it is... Wait a minute, why would you fasten one of these to the floor in the storage room? You can’t use it like this."

# game/invest.rpy:69
translate Francais invest_4_5ea8c8fc:

    # mi "Maybe the construction workers messed up the order? That can happen sometimes."
    mi "Maybe the construction workers messed up the order? That can happen sometimes."

# game/invest.rpy:70
translate Francais invest_4_46e5227a:

    # p "Ah, bureaucracy: the arch nemesis of efficiency. In any case, let's check something else."
    p "Ah, bureaucracy: the arch nemesis of efficiency. In any case, let's check something else."

# game/invest.rpy:76
translate Francais invest_5_b7392f34:

    # p "Hm... there’s some sort of substance on the fire extinguisher."
    p "Hm... there’s some sort of substance on the fire extinguisher."

# game/invest.rpy:77
translate Francais invest_5_16ba9f81:

    # p "Minami-chan, come look at this: there is some kind of weird pink powder on the extinguisher."
    p "Minami-chan, come look at this: there is some kind of weird pink powder on the extinguisher."

# game/invest.rpy:79
translate Francais invest_5_6577988f:

    # mi "Huh, it must be leaking. It’s probably pretty old..."
    mi "Huh, it must be leaking. It’s probably pretty old..."

# game/invest.rpy:80
translate Francais invest_5_c713c3c5:

    # p "That’s possible, but is fire retardant pink?"
    p "That’s possible, but is fire retardant pink?"

# game/invest.rpy:81
translate Francais invest_5_c83f5e10:

    # mi "No idea. Pink is an adorable color, so maybe it stops fires with cuteness ^^."
    mi "No idea. Pink is an adorable color, so maybe it stops fires with cuteness ^^."

# game/invest.rpy:82
translate Francais invest_5_9322b3cf:

    # p "(The pressure gauge is fine, and a leak from the bottom would have made it explode… something about this doesn’t add up.)"
    p "(The pressure gauge is fine, and a leak from the bottom would have made it explode… something about this doesn’t add up.)"

# game/invest.rpy:83
translate Francais invest_5_97a69a07:

    # p "(Nothing to be done about it right now, though. I will keep this in mind.)"
    p "(Nothing to be done about it right now, though. I will keep this in mind.)"

# game/invest.rpy:90
translate Francais invest_end_e7bde75e:

    # mi "Aw, such a letdown! I was expecting to find something interesting..."
    mi "Aw, such a letdown! I was expecting to find something interesting..."

# game/invest.rpy:91
translate Francais invest_end_eeecac80:

    # p "Me too, but none of this explains the mystery of where Harada-sensei and the volleyballs went."
    p "Me too, but none of this explains the mystery of where Harada-sensei and the volleyballs went."

# game/invest.rpy:93
translate Francais invest_end_781d0dff:

    # mi "Anyway, it's gotten pretty late. If you want to speak to my dad before he gets… ‘sleepy’, we’d better get going."
    mi "Anyway, it's gotten pretty late. If you want to speak to my dad before he gets… ‘sleepy’, we’d better get going."

# game/invest.rpy:94
translate Francais invest_end_03c476bb:

    # p "Sounds good. I wouldn’t want your father to worry, or think that we have gotten up to some sort of mischief."
    p "Sounds good. I wouldn’t want your father to worry, or think that we have gotten up to some sort of mischief."

# game/invest.rpy:95
translate Francais invest_end_00383fdc:

    # mi "(I wouldn’t mind a little mischief… It’s been awhile since [player_name]-kun ‘played’ with me…)"
    mi "(I wouldn’t mind a little mischief… It’s been awhile since [player_name]-kun ‘played’ with me…)"

# game/invest.rpy:97
translate Francais invest_end_9eed01f9:

    # "You quickly leave the school and head to Minami’s place."
    "You quickly leave the school and head to Minami’s place."

