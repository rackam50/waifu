﻿# TODO: Translation updated at 2019-11-27 12:19

translate Francais strings:

    # game/screens.rpy:262
    old "Back"
    new "Back"

    # game/screens.rpy:263
    old "History"
    new "History"

    # game/screens.rpy:264
    old "Skip"
    new "Skip"

    # game/screens.rpy:265
    old "Auto"
    new "Auto"

    # game/screens.rpy:266
    old "Save"
    new "Save"

    # game/screens.rpy:267
    old "Q.Save"
    new "Q.Save"

    # game/screens.rpy:268
    old "Q.Load"
    new "Q.Load"

    # game/screens.rpy:269
    old "Options"
    new "Options"

    # game/screens.rpy:460
    old "Extra"
    new "Extra"

    # game/screens.rpy:696
    old "About"
    new "About"

    # game/screens.rpy:703
    old "Version [config.version!t]\n"
    new "Version [config.version!t]\n"

    # game/screens.rpy:709
    old "Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"
    new "Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"

    # game/screens.rpy:744
    old "Load"
    new "Load"

    # game/screens.rpy:749
    old "Page {}"
    new "Page {}"

    # game/screens.rpy:749
    old "Automatic saves"
    new "Automatic saves"

    # game/screens.rpy:749
    old "Quick saves"
    new "Quick saves"

    # game/screens.rpy:814
    old "{#file_time}%A, %B %d %Y, %H:%M"
    new "{#file_time}%A, %B %d %Y, %H:%M"

    # game/screens.rpy:814
    old "empty slot"
    new "empty slot"

    # game/screens.rpy:835
    old "<"
    new "<"

    # game/screens.rpy:842
    old "{#auto_page}A"
    new "{#auto_page}A"

    # game/screens.rpy:849
    old "{#quick_page}Q"
    new "{#quick_page}Q"

    # game/screens.rpy:863
    old ">"
    new ">"

    # game/screens.rpy:926
    old "Display"
    new "Display"

    # game/screens.rpy:927
    old "Window"
    new "Window"

    # game/screens.rpy:928
    old "Fullscreen"
    new "Fullscreen"

    # game/screens.rpy:932
    old "Rollback Side"
    new "Rollback Side"

    # game/screens.rpy:933
    old "Disable"
    new "Disable"

    # game/screens.rpy:934
    old "Left"
    new "Left"

    # game/screens.rpy:935
    old "Right"
    new "Right"

    # game/screens.rpy:940
    old "Unseen Text"
    new "Unseen Text"

    # game/screens.rpy:941
    old "After Choices"
    new "After Choices"

    # game/screens.rpy:942
    old "Transitions"
    new "Transitions"

    # game/screens.rpy:955
    old "Text Speed"
    new "Text Speed"

    # game/screens.rpy:959
    old "Auto-Forward Time"
    new "Auto-Forward Time"

    # game/screens.rpy:966
    old "Music Volume"
    new "Music Volume"

    # game/screens.rpy:973
    old "Sound Volume"
    new "Sound Volume"

    # game/screens.rpy:979
    old "Test"
    new "Test"

    # game/screens.rpy:983
    old "Voice Volume"
    new "Voice Volume"

    # game/screens.rpy:994
    old "Mute All"
    new "Mute All"

    # game/screens.rpy:1110
    old "The dialogue history is empty."
    new "The dialogue history is empty."

    # game/screens.rpy:1171
    old "Help"
    new "Help"

    # game/screens.rpy:1180
    old "Keyboard"
    new "Keyboard"

    # game/screens.rpy:1181
    old "Mouse"
    new "Mouse"

    # game/screens.rpy:1184
    old "Gamepad"
    new "Gamepad"

    # game/screens.rpy:1197
    old "Enter"
    new "Enter"

    # game/screens.rpy:1198
    old "Advances dialogue and activates the interface."
    new "Advances dialogue and activates the interface."

    # game/screens.rpy:1201
    old "Space"
    new "Space"

    # game/screens.rpy:1202
    old "Advances dialogue without selecting choices."
    new "Advances dialogue without selecting choices."

    # game/screens.rpy:1205
    old "Arrow Keys"
    new "Arrow Keys"

    # game/screens.rpy:1206
    old "Navigate the interface."
    new "Navigate the interface."

    # game/screens.rpy:1209
    old "Escape"
    new "Escape"

    # game/screens.rpy:1210
    old "Accesses the game menu."
    new "Accesses the game menu."

    # game/screens.rpy:1213
    old "Ctrl"
    new "Ctrl"

    # game/screens.rpy:1214
    old "Skips dialogue while held down."
    new "Skips dialogue while held down."

    # game/screens.rpy:1217
    old "Tab"
    new "Tab"

    # game/screens.rpy:1218
    old "Toggles dialogue skipping."
    new "Toggles dialogue skipping."

    # game/screens.rpy:1221
    old "Page Up"
    new "Page Up"

    # game/screens.rpy:1222
    old "Rolls back to earlier dialogue."
    new "Rolls back to earlier dialogue."

    # game/screens.rpy:1225
    old "Page Down"
    new "Page Down"

    # game/screens.rpy:1226
    old "Rolls forward to later dialogue."
    new "Rolls forward to later dialogue."

    # game/screens.rpy:1230
    old "Hides the user interface."
    new "Hides the user interface."

    # game/screens.rpy:1234
    old "Takes a screenshot."
    new "Takes a screenshot."

    # game/screens.rpy:1238
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # game/screens.rpy:1244
    old "Left Click"
    new "Left Click"

    # game/screens.rpy:1248
    old "Middle Click"
    new "Middle Click"

    # game/screens.rpy:1252
    old "Right Click"
    new "Right Click"

    # game/screens.rpy:1256
    old "Mouse Wheel Up\nClick Rollback Side"
    new "Mouse Wheel Up\nClick Rollback Side"

    # game/screens.rpy:1260
    old "Mouse Wheel Down"
    new "Mouse Wheel Down"

    # game/screens.rpy:1267
    old "Right Trigger\nA/Bottom Button"
    new "Right Trigger\nA/Bottom Button"

    # game/screens.rpy:1271
    old "Left Trigger\nLeft Shoulder"
    new "Left Trigger\nLeft Shoulder"

    # game/screens.rpy:1275
    old "Right Shoulder"
    new "Right Shoulder"

    # game/screens.rpy:1280
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # game/screens.rpy:1284
    old "Start, Guide"
    new "Start, Guide"

    # game/screens.rpy:1288
    old "Y/Top Button"
    new "Y/Top Button"

    # game/screens.rpy:1291
    old "Calibrate"
    new "Calibrate"

    # game/screens.rpy:1403
    old "Skipping"
    new "Skipping"

    # game/screens.rpy:1624
    old "Menu"
    new "Menu"

